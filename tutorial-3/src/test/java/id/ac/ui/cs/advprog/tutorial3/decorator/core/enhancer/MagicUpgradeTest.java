package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Longbow", magicUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertTrue(magicUpgrade.getDescription().contains("with magic upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        Longbow longbow = new Longbow();
        assertTrue(magicUpgrade.getWeaponValue() > longbow.getWeaponValue());
        assertTrue(magicUpgrade.getWeaponValue() <= longbow.getWeaponValue() + 20);
    }
}
