package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member newMember = new OrdinaryMember("Fulan", "Servant");
        guild.addMember(guildMaster, newMember);
        assertEquals(2, guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        Member newMember = new OrdinaryMember("Fulan", "Servant");
        guild.addMember(guildMaster, newMember);
        assertEquals(2, guild.getMemberList().size());
        guild.removeMember(guildMaster, newMember);
        assertEquals(1, guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member newMember = new OrdinaryMember("Fulan", "Servant");
        guild.addMember(guildMaster, newMember);
        assertEquals(newMember, guild.getMember("Fulan", "Servant"));
    }
}
