package id.ac.ui.cs.advprog.tutorial3.composite.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member childMember = new OrdinaryMember("Fulan", "Merchant");
        member.addChildMember(childMember);
        assertEquals(1, member.getChildMembers().size());
        assertEquals(childMember, member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member childMember = new OrdinaryMember("Fulan", "Merchant");
        member.addChildMember(childMember);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(childMember);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member member1 = new OrdinaryMember("Fulan", "Merchant");
        Member member2 = new OrdinaryMember("Fulana", "Merchant");
        Member member3 = new OrdinaryMember("Fulanb", "Merchant");
        Member member4 = new OrdinaryMember("Fulanc", "Merchant");
        member.addChildMember(member1);
        member.addChildMember(member2);
        member.addChildMember(member3);
        assertEquals(3, member.getChildMembers().size());
        member.addChildMember(member4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member guildMaster = new PremiumMember("Eko", "Master");
        Member member1 = new OrdinaryMember("Fulan", "Merchant");
        Member member2 = new OrdinaryMember("Fulana", "Merchant");
        Member member3 = new OrdinaryMember("Fulanb", "Merchant");
        Member member4 = new OrdinaryMember("Fulanc", "Merchant");
        guildMaster.addChildMember(member1);
        guildMaster.addChildMember(member2);
        guildMaster.addChildMember(member3);
        assertEquals(3, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(member4);
        assertEquals(4, guildMaster.getChildMembers().size());
    }
}
