package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Nina", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertTrue(member.getRole().equals("Merchant"));
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member anotherMember = new OrdinaryMember("Fulan", "Merchant");
        member.addChildMember(anotherMember);
        assertEquals(0, member.getChildMembers().size());
        member.removeChildMember(anotherMember);
        assertEquals(0, member.getChildMembers().size());
    }
}
