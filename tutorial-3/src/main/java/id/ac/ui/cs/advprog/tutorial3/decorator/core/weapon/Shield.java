package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        public Shield() {
                weaponName = "Shield";
                weaponDescription = "This is Shield";
                weaponValue = 10;
        }
}
