package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        public Longbow() {
                weaponName = "Longbow";
                weaponDescription = "This is Longbow";
                weaponValue = 15;
        }
}
