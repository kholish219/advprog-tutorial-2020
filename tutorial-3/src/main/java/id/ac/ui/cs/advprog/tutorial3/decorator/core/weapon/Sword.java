package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        public Sword() {
                weaponName = "Sword";
                weaponDescription = "This is Sword";
                weaponValue = 25;
        }
}
