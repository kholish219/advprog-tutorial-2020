package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
        String attackType = "Gun";

        public String attack() {
                return "Attack with " + attackType;
        }

        public String getType() {
                return attackType;
        }
}
