package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
        String attackType = "Magic";

        public String attack() {
                return "Attack with " + attackType;
        }

        public String getType() {
                return attackType;
        }
}
