package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
        String defendType = "Barrier";

        public String defend() {
                return "Defend with " + defendType;
        }
        
        public String getType() {
                return defendType;
        }
}
