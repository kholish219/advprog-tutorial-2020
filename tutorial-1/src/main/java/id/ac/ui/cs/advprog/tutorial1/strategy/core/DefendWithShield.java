package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
        String defendType = "Shield";

        public String defend() {
                return "Defend with " + defendType;
        }
        
        public String getType() {
                return defendType;
        }
}
