package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
        String defendType = "Armor";

        public String defend() {
                return "Defend with " + defendType;
        }
        
        public String getType() {
                return defendType;
        }
}
