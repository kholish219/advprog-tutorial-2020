package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
        String attackType = "Sword";

        public String attack() {
                return "Attack with " + attackType;
        }

        public String getType() {
                return attackType;
        }
}
