package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
        this.name = "majestic";
    }

    @Override
    public void prepare() {
        this.armor = armory.craftArmor();
        this.weapon = armory.craftWeapon();
    }

    @Override
    public String getDescription() {
        return "Majestic Knight";
    }
}
