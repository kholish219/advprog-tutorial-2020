package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
        this.name = "synthetic";
    }

    @Override
    public void prepare() {
        this.weapon = armory.craftWeapon();
        this.skill = armory.learnSkill();
    }

    @Override
    public String getDescription() {
        return "Synthetic Knight";
    }
}
