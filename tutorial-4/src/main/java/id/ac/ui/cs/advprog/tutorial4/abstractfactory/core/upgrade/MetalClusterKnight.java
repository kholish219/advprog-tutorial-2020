package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
        this.name = "metal cluster";
    }

    @Override
    public void prepare() {
        this.armor = armory.craftArmor();
        this.skill = armory.learnSkill();
    }

    @Override
    public String getDescription() {
        return "Metal Cluster Knight";
    }
}
