package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    HolyGrail holyGrail;

    @BeforeEach
    public void setUp() {
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAWish() {
        holyGrail.makeAWish("wish me luck");
        assertEquals("wish me luck", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetHolyWish() {
        assertNotNull(holyGrail.getHolyWish());
    }
}
