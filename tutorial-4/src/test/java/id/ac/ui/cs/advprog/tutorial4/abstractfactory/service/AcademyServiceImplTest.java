package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @Test
    public void testProduceKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertNotEquals(0, academyService.getKnightAcademies().size());

        academyService.produceKnight("Lordran", "majestic");

        assertNotNull(academyService.getKnight());
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallGetKnightAcademies(){
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }

    
}
